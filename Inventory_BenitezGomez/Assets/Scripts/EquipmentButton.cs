﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using GameManagerScript;
using InventoryManagerScript;

namespace EquipmentButtonScript
{
    public class EquipmentButton : MonoBehaviour, IPointerClickHandler
    {
        public Item item;
        public static event Action<Item> onEquipmentRightClick;

        public void OnPointerClick(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Right)
            {
                if (item != null && GameManager.Get().GetCurrentSize() < GameManager.Get().GetSizeInventory())
                {
                    onEquipmentRightClick?.Invoke(item);
                    GameManager.Get().AddItem(item);
                    InventoryManager.OnSave?.Invoke();
                    item = null;
                }
            }
        }
    }
}