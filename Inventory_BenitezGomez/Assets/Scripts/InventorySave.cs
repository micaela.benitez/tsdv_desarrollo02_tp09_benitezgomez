﻿using UnityEngine;
using GameManagerScript;
using InventoryManagerScript;
using ItemManagerScript;
using EquipmentManagerScript;

namespace InventorySaveScript
{
    public class InventorySave : MonoBehaviour
    {
        public InventoryData _data = new InventoryData();
        public ItemManager items;
        public EquipmentManager loadEquipment;
        private string json;
        private bool deleteSave;

        private void Awake()
        {
            InventoryManager.OnSave += GetInventory;
            InventoryManager.OnSave += GetEquipment;
        }

        private void Start()
        {
            if (!PlayerPrefs.HasKey("Equip&Inventory")) return;
            json = PlayerPrefs.GetString("Equip&Inventory");
            _data = JsonUtility.FromJson<InventoryData>(json);

            SetInventory();
            SetEquipment();
            GameManager.Get().CurrentSize(_data.totalSizeInventory);
        }

        private void OnApplicationQuit()
        {
            _data.totalSizeInventory = GameManager.Get().GetCurrentSize();
            json = JsonUtility.ToJson(_data);
            PlayerPrefs.SetString("Equip&Inventory", json);
            PlayerPrefs.Save();

            if (!deleteSave) return;
            PlayerPrefs.DeleteKey("Equip&Inventory");
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.R))
                deleteSave = true;
        }

        private void OnDisable()
        {
            InventoryManager.OnSave -= GetInventory;
            InventoryManager.OnSave -= GetEquipment;
        }

        public void GetInventory()
        {
            int i = 0;
            Clean(_data.idItem);
            foreach (Item item in GameManager.Get().inventory)
            {
                _data.idItem[i] = item.Id;
                i++;
            }
        }

        public void GetEquipment()
        {
            Clean(_data.idEquipment);
            for (int i = 0; i < _data.idEquipment.Length; i++)
            {
                if (GameManager.Get().equipment[i] != null)
                    _data.idEquipment[i] = GameManager.Get().equipment[i].Id;
            }
        }

        public void SetInventory()
        {
            for (int i = 0; i < _data.idItem.Length; i++)
            {
                if (_data.idItem[i] != 0)
                    GameManager.Get().AddItem(items.GetItem(_data.idItem[i]));
            }
        }

        public void SetEquipment()
        {
            for (int i = 0; i < _data.idEquipment.Length; i++)
            {
                if (_data.idEquipment[i] != 0)
                {
                    GameManager.Get().equipment[i] = items.GetItem(_data.idEquipment[i]);
                    loadEquipment.LoadItem(GameManager.Get().equipment[i], (EquipmentType)i);
                }
            }
        }
        public void Clean(int[] _arr) // pediente de cambio 
        {
            for (int i = 0; i < _arr.Length; i++)
                _arr[i] = 0;
        }
    }
}