﻿using System;
using TMPro;
using UnityEngine;

namespace TooltipScript
{
    public class Tooltip : MonoBehaviour
    {
        public void SetTooltipText(String _text)
        {
            GetComponentInChildren<TMP_Text>().text = _text;
        }
    }
}