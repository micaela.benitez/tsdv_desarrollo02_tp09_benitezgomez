﻿using System;

[Serializable]
public class InventoryData 
{
    public int[] idItem = new int[15];
    public int[] idEquipment = new int[4];
    public int totalSizeInventory;
}
