﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ItemHoverScript
{
    public class ItemHover : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public String itemDescription;
        public static event Action<String> onItemHover;
        public static event Action onItemStopHover;

        public void OnPointerEnter(PointerEventData eventData)
        {
            onItemHover?.Invoke(itemDescription);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            onItemStopHover?.Invoke();
        }
    }
}