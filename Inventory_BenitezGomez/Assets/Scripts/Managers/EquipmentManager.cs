﻿using UnityEngine;
using UnityEngine.UI;
using ItemButtonScript;
using EquipmentButtonScript;
using GameManagerScript;
using ItemHoverScript;

namespace EquipmentManagerScript
{
    public enum EquipmentType
    {
        HELMET,
        CHEST,
        BOOTS,
        WEAPON
    }

    public class EquipmentManager : MonoBehaviour
    {        
        public GameObject[] slots;

        private void OnEnable()
        {
            ItemButton.onItemRightClick += EquipItem;
            EquipmentButton.onEquipmentRightClick += UnequipItem;
        }

        private void OnDisable()
        {
            ItemButton.onItemRightClick -= EquipItem;
            EquipmentButton.onEquipmentRightClick -= UnequipItem;
        }

        private void EquipItem(Item _item)
        {
            if (_item.typeItem == Item.TypeItem.WEAPON)
            {
                CheckItem(EquipmentType.WEAPON);
                LoadItem(_item, EquipmentType.WEAPON);
            }
            else if (_item.typeItem == Item.TypeItem.ARMOR)
            {
                ItemArmor _itemArmor = (ItemArmor)_item;
                switch (_itemArmor.subTypeItem)
                {
                    case ItemArmor.SubTypeItem.HELMET:
                        CheckItem(EquipmentType.HELMET);
                        LoadItem(_item, EquipmentType.HELMET);
                        break;
                    case ItemArmor.SubTypeItem.CHEST:
                        CheckItem(EquipmentType.CHEST);
                        LoadItem(_item, EquipmentType.CHEST);
                        break;
                    case ItemArmor.SubTypeItem.BOOTS:
                        CheckItem(EquipmentType.BOOTS);
                        LoadItem(_item, EquipmentType.BOOTS);
                        break;
                }
            }
        }

        private void CheckItem(EquipmentType type)
        {
            if (GameManager.Get().equipment[(int)type] != null)
                GameManager.Get().AddItem(GameManager.Get().equipment[(int)type]);
        }

        public void LoadItem(Item _item, EquipmentType type)
        {
            GameManager.Get().equipment[(int)type] = _item;
            slots[(int)type].GetComponent<Image>().sprite = _item.icon;
            slots[(int)type].GetComponent<Image>().color = Color.white;
            slots[(int)type].GetComponent<EquipmentButton>().item = _item;
            slots[(int)type].GetComponent<EquipmentButton>().item.SetItemText(ref slots[(int)type].GetComponent<ItemHover>().itemDescription);
        }

        private void UnequipItem(Item _item)
        {
            if (_item.typeItem == Item.TypeItem.WEAPON)
            {
                QuitItem(EquipmentType.WEAPON);
            }
            else if (_item.typeItem == Item.TypeItem.ARMOR)
            {
                ItemArmor _itemArmor = (ItemArmor)_item;
                switch (_itemArmor.subTypeItem)
                {
                    case ItemArmor.SubTypeItem.HELMET:
                        QuitItem(EquipmentType.HELMET);
                        break;
                    case ItemArmor.SubTypeItem.CHEST:
                        QuitItem(EquipmentType.CHEST);
                        break;
                    case ItemArmor.SubTypeItem.BOOTS:
                        QuitItem(EquipmentType.BOOTS);
                        break;
                }
            }
        }

        private void QuitItem(EquipmentType type)
        {
            GameManager.Get().equipment[(int)type] = null;
            slots[(int)type].GetComponent<Image>().sprite = null;
            slots[(int)type].GetComponent<Image>().color = Color.black;
        }
    }
}