﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace ItemManagerScript
{
    public class ItemManager : MonoBehaviour
    {
        public List<Item> items;

        private void Awake()
        {
            setIdItem();
        }

        private void setIdItem()
        {   
            int aux = 1;
            foreach (Item item in items)
            {
                item.Id = aux;
                aux++;
            }
        }

        public Item GetItem(int _id)
        {
            return _id == 0 ? items[_id] : items[_id - 1];
        }

        public Item SelectedItem()
        {
            int randomAux = Random.Range(0, items.Count);

            return items[randomAux];
        }
    }
}