﻿using System;
using UnityEngine;
using GameManagerScript;
using ItemManagerScript;
using ItemHoverScript;
using TooltipScript;

namespace InventoryManagerScript
{
    public class InventoryManager : MonoBehaviour
    {
        public GameObject tooltip;
        private ItemManager item;        
        
        public delegate void Save();
        public static Save OnSave;
        private void Awake()
        {
            item = FindObjectOfType<ItemManager>();
        }

        private void OnEnable()
        {
            ItemHover.onItemHover += ShowTooltip;
            ItemHover.onItemStopHover += HideTooltip;
        }

        private void OnDisable()
        {
            ItemHover.onItemHover -= ShowTooltip;
            ItemHover.onItemStopHover -= HideTooltip;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                if (GameManager.Get().GetCurrentSize() < GameManager.Get().GetSizeInventory())
                {
                    Item newItem = item.SelectedItem();
                    GameManager.Get().AddItem(newItem);
                    OnSave?.Invoke();
                }
            }
        }

        private void ShowTooltip(String _string)
        {
            tooltip.GetComponent<Tooltip>().SetTooltipText(_string);
            tooltip.SetActive(true);
        }

        private void HideTooltip()
        {
            tooltip.SetActive(false);
        }
    }
}