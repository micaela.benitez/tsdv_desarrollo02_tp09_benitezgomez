﻿using System;
using System.Collections.Generic;
using UnityEngine;
using MonoBehaviourSingletonScript;
using ItemButtonScript;
using ItemHoverScript;

namespace GameManagerScript
{
    public class GameManager : MonoBehaviourSingleton<GameManager>
    {
        public GameObject itemButtonPrefab;
        public Transform container;

        public List<Item> inventory = new List<Item>();
        public Item[] equipment = new Item[4];

        private int sizeInventory = 15;
        private int currentSize;

        public void AddItem(Item _item)
        {
            inventory.Add(_item);
            GameObject go = Instantiate(itemButtonPrefab, Vector3.zero, Quaternion.identity, container);

            go.GetComponent<ItemButton>().item = _item;
            _item.SetItemText(ref go.GetComponent<ItemHover>().itemDescription);
            currentSize++;
        }

        public void RemoveItem(Item _item)
        {
            inventory.Remove(_item);
            currentSize--;
        }

        public void CurrentSize(int num)
        {
            currentSize = num;
        }

        public int GetSizeInventory()
        {
            return sizeInventory;
        }

        public int GetCurrentSize()
        {
            return currentSize;
        }
    }
}