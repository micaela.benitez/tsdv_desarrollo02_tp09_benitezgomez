﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using GameManagerScript;
using InventoryManagerScript;

namespace ItemButtonScript
{
    public class ItemButton : MonoBehaviour, IPointerClickHandler
    {
        public Item item;
        public static event Action<Item> onItemRightClick;

        private void Start()
        {
            GetComponentInChildren<Image>().sprite = item.icon;
            GetComponentInChildren<Text>().text = item.itemName;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (eventData.button != PointerEventData.InputButton.Right) return;
            onItemRightClick?.Invoke(item);
            GameManager.Get().RemoveItem(item);
            InventoryManager.OnSave?.Invoke();
            Destroy(this.gameObject);
        }
    }
}