﻿using UnityEngine;

public class Item : ScriptableObject
{
    public Sprite icon;
    public GameObject model;
    public string itemName;
    public int weight;
    public int durability;
    public int level;
    private int id;

    public int Id { get { return id;} set { id = value;}}

    public enum TypeItem { WEAPON, ARMOR }
    public TypeItem typeItem;
    
    public virtual void SetItemText(ref string _text) { }
}
