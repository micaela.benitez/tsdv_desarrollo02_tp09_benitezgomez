﻿using UnityEngine;

[CreateAssetMenu(fileName = "Armor", menuName = "Armor")]
public class ItemArmor : Item
{
    public int defenseValue;
    public enum SubTypeItem { HELMET, CHEST, BOOTS }
    public SubTypeItem subTypeItem;

    public override void SetItemText(ref string _text)
    {
        _text = itemName + "\n" +
                "Weight: " + weight + "\n" +
                "Durability: " + durability + "\n" +
                "Level: " + level + "\n" +
                "Defense: " + defenseValue;
    }
}