﻿using UnityEngine;

[CreateAssetMenu(fileName = "Waepon", menuName = "Weapon")]
public class ItemWeapon : Item
{
    public int damage;
    public enum SubTypeItem { AXE, DAGGER, SWORD }
    public SubTypeItem subTypeItem;

    public override void SetItemText(ref string _text)
    {
        _text = itemName + "\n" +
                "Weight: " + weight + "\n" +
                "Durability: " + durability + "\n" +
                "Level: " + level + "\n" +
                "Damage: " + damage;
    }
}